    to_be_or_not_to_be0
    ^
    p
  start

    to_be_or_not_to_be0
    ^ p    => copie des caractères words[n] (aller la mémoire !)
              de start à p (p - start chars !)
  start       ne pas oublier d'ajouter le 0 terminal

   n augmente de 1 ; p augmente de 1 ; start est initialisé à p
   et on continue...

   attention à bien s'arrêter à la fin de la chaîne et à ne pas
   oublier le dernier mot.
