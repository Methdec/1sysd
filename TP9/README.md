# Découper une phrase en mots

Écrire une fonction `int split(char *msg, char *words[])` qui
découpe la chaîne `msg` en mots qui seront copiés dans le
tableau de chaîne `words`, la fonction renvoiant le nombre
de mots identifiés.

Dans un premier temps on suppose que `msg` est constitués
d'une suite de mots séparés par un seul espace, on traitera
la question d'espaces multiples ensuite.

Pour tester la fonction faites en sorte que votre programme
`split` passe son premier argument à la fonction split et
affiche le résultat :

~~~~Bash
$ ./split "to be or not to be"
6 mots :
to
be
or
not
to
be
$
~~~~

# Le jeu de la vie de Conway dans un terminal

Vous pouvez connaître la taille de votre terminal avec
`tput cols` et `tput lines`. Le programme utilisera un
tableau à deux dimensions de caractères (espace ou ' ')
de taille légèrement inférieure à votre terminal, 79x23
par exemple pour un terminal à 80 colonnes et 24 lignes :

~~~~C
#define LINES 23
#define COLS 79
~~~~

Écrire une fonction `init_world(char world[LINES][COLS])` qui
initialise aléatoirement les cases "vivantes" et "morte" (une
chance sur deux).

Écrire une fonction `display_world(char world[LINES][COLS])` qui
affiche les cellules.

La fonction de passage d'une étape à la suivante 
`update_world(char world[LINES][COLS])` recevra
le tableau actuel, remplira un nouveau tableau de même taille
avec les cellules de la génération suivante puis copiera ce
tableau dans le premier (cellule par cellule).

Vous pouvez écrire une fonction implémentant ces règles
`int neighborhood(char wold[LINES][COLS], int x, int y)`
qui compte le nombre de voisines vivantes d'une cellule
placé à la ligne y et la colonne x. 

Rappel des règles : le voisinage d'une cellule est constituée
des 8 cellules qui l'entourent. Si une cellule est vivante
elle survit si elle a 2 ou 3 voisines vivantes, si une case
est vide une cellule vivante "naît" si elle a exactement 3
voisines vivantes.

Il y a deux façon de gérer les bords : identifier les côtés
opposés ou considérer que hors du tableau les cellules sont
"mortes".

Dans le programme principal `main` vous pouvez alors voir le
jeu de la vie en action :

~~~~C
int main() {
    char world[LINES][COLS];

    init_world(world);
    while (1) { // ctrl-C pour quitter
        display_world(world);
        sleep(1);
        update_world(world);
        system("clear"); // efface le terminal (commande UNIX)
    }
}
~~~~
