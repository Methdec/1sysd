#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<unistd.h>

#define LINES 15
#define COLS 73

void init_world(char world[LINES][COLS]) {
    for (int i = 0; i < LINES; i++) {
        for (int j = 0; j < COLS; j++) {
            if (rand() % 2 == 0) {
                world[i][j] = ' ';
            } else {
                world[i][j] = '*';
            }
        }
    }
}

void display_world(char world[LINES][COLS]) {
    for (int i = 0; i < LINES; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%c", world[i][j]);
        }
        printf("\n");
    }
}



int main() {
    char world[LINES][COLS];

    srand(time(NULL));

    init_world(world);
    while (1) { // ctrl-C pour quitter
        display_world(world);
        sleep(1);
        //update_world(world);
        system("clear"); // efface le terminal (commande UNIX)
    }
}
