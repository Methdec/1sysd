#include<stdio.h>
#include<stdlib.h>

float inches2cm(float val){
	val = val*2.54;
	return val;
}

float cm2inches(float val){
	val = val/2.54;
	return val;
}
int main() {
int choix = 0;
float val = 0;

printf("Pouces vers cm 1 ou cm vers pouces 2 \n");
scanf("%d", &choix);
printf("Quelle valeur voulez-vous convertir ? \n");
scanf("%f", &val);

if (choix == 1){
	val = inches2cm(val);
	printf("%.2f\n", val);
}
else {
	val = cm2inches(val);
	printf("%.2f\n",val);
}
}
