# Traitement de la ligne de commande

## Une calculatrice

Écrire un programme qui attend en argument deux nombres et
une opération et affiche le résultat.

- Vous aurez besoin : de switch/case, de strtof ou strtod
- Note l'argument qui contient un opérateur sera une chaîne
 de caractère, il faudra extraire ce caractère

Écrire un programme qui somme la suite des arguments qu'il a 
reçu

Bonus : écrire un programme qui attend une suite de mots en
arguments et les affiche triés dans l'ordre alphabétique
(lexicographique, sans gérer les accents). Vous aurez besoin
de `strcmp` et pourrez reprendre le code de `TP3/tri.c`.

- Mettre la fonction de tri dans une fonction
`void sort_tabstr(char *tab[])`
- Dans `main` extraire dans un tableau `t` la partie
de `argv` qui nous intéresse
- Faire en sorte que le programme s'arrête avec un rappel
de son fonctionnement si le nombre d'arguments est 0

