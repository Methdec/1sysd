#include<stdio.h>

// version avec des indices i et j
int is_palindrome1(char *s) {
    int i = 0, j = 0;
    while (s[j]) { // jusqu'au zéro terminal
        j++;
    }; 
    // s[j] est le zéro terminal, un cran trop loin : on recule
    j--;
    // on i est au début (0) j à la fin (dernier caractère)

    while ( i < j && s[i] == s[j] ) {
        i++;
        j--;
    }
    return s[i] == s[j]; // on est sorti parce que i et j se sont rejoints
}

// version avec des pointeurs p et q
int is_palindrome2(char *s) {
    char *p, *q;
    p = q = s;
    while (*q) {
        q++;
    };
    // ici : q pointe sur le zéro terminal de s, on recule !
    q--;
    // go !
    while ( p < q && *p == *q ) {
        p++;
        q--; 
    }
    return *p == *q;
}

int main() {
    // tableau de chaînes
    char *tests[] = { "radar", "serres", "abcd", "serves", "radir" };
    int i;

    for ( i = 0; i < 5; i++) {
        printf("%s\t", tests[i]);
        if (is_palindrome1(tests[i])) {
            printf("OUI (palindrome1) ");
        } else {
            printf("NON (palindrome1) ");
        }
        if (is_palindrome2(tests[i])) {
            printf("OUI (palindrome2)\n");
        } else {
            printf("NON (palindrome2)\n");
        }
    }
}
