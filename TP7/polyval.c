#include<stdio.h>


struct Poly {
    int degree;
    double coeffs[50]; // deg max 49
}



double power(double x, int n) {
    double result = 1;
    for (int i = 0; i < n; i++) {
        result *= x;
    }
    return result;
}



double pvalue(struct Poly p, double x){
	int i;
	int deg;
	int result;
	deg = Poly p[0];

	while (deg >= 0){
		result = result + p[1][deg]*power(x, deg);
		
		deg--;
	}
	return x;
}

int main() {
    struct Poly p1 = { 2, { 3, 2, 1 } };     // x^2 + 2x + 3
    struct Poly p2 = { 3, { 1, 0, -1, 2 } }; // 2x^3 - x^2 + 2
    			//

    printf("%d %.2lf\n", p1.degree, p1.coeffs[2]);
    printf("%d", pvalue(p1, 1));
}

